function checkOib(oibCode) {
    var checkDigit, num;
    console.log(oibCode.toString());

    var code = oibCode.toString();

    if (code.length === 13 && code.substr(0, 2).toUpperCase() === 'HR') {
        code = code.substr(2, 11);
    } else if (code.length !== 11) {
        return false;
    }

    var numCheck = parseInt(code, 10);
    if (isNaN(numCheck)) {
        return false;
    }

    num = 10;
    for (var i = 0; i < 10; i++) {
        num = num + parseInt(code.substr(i, 1), 10);
        num = num % 10;
        if (num === 0) {
            num = 10;
        }
        num *= 2;
        num = num % 11;
    }

    checkDigit = 11 - num;
    if (checkDigit === 10) {
        checkDigit = 0;
    }

    return parseInt(code.substr(10, 1), 10) === checkDigit;
}


// Get the input box
function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}


$('#student-oib').on('change blur keydown', delay(function (e) {
    $('#student-form').yiiActiveForm('find', 'student-oib').validate = function (attribute, value, messages, deferred, $form) {
        if (!checkOib($('#student-oib').val())){
            $('#student-form').yiiActiveForm('updateAttribute', 'student-oib', ["Oib is not valid"]);
        } else {
            $('#student-form').yiiActiveForm('updateAttribute', 'student-oib', '');
        }
    }
}, 300));

$('#professor-oib').on('change blur keydown', delay(function (e) {
    $('#professor-form').yiiActiveForm('find', 'professor-oib').validate = function (attribute, value, messages, deferred, $form) {
        if (!checkOib($('#professor-oib').val())){
            $('#professor-form').yiiActiveForm('updateAttribute', 'professor-oib', ["Oib is not valid"]);
        } else {
            $('#professor-form').yiiActiveForm('updateAttribute', 'professor-oib', '');
        }
    }
}, 300));