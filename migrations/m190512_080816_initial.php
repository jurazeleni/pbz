<?php

use yii\db\Migration;

/**
 * Class m190512_080816_initial
 */
class m190512_080816_initial extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $tables = Yii::$app->db->schema->getTableNames();
        $dbType = $this->db->driverName;
        $tableOptions_mysql = "CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB";
        /* MYSQL */
        if (!in_array('exam', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%exam}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'student_id' => 'INT(11) NOT NULL',
                    'professor_id' => 'INT(11) NOT NULL',
                    'subject_id' => 'INT(11) NOT NULL',
                    'mark' => 'INT(1) NOT NULL',
                    'date' => 'DATE NOT NULL',
                ], $tableOptions_mysql);
            }
        }

        /* MYSQL */
        if (!in_array('professor', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%professor}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'oib' => 'VARCHAR(11) NOT NULL',
                    'name' => 'VARCHAR(255) NOT NULL',
                    'surname' => 'VARCHAR(255) NOT NULL',
                    'address' => 'VARCHAR(255) NULL',
                    'post' => 'INT(11) NULL',
                    'location' => 'VARCHAR(255) NULL',
                    'date_of_birth' => 'DATE NULL',
                ], $tableOptions_mysql);
            }
        }

        /* MYSQL */
        if (!in_array('subject', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%subject}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'name' => 'VARCHAR(255) NOT NULL',
                    'ects_points' => 'INT(4) NOT NULL',
                    'active' => 'ENUM(\'active\',\'inactive\') NOT NULL DEFAULT \'active\'',
                ], $tableOptions_mysql);
            }
        }

        /* MYSQL */
        if (!in_array('professor_subject_link', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%professor_subject_link}}', [
                    'professor_id' => 'INT(11) NOT NULL',
                    'subject_id' => 'INT(11) NOT NULL',
                ], $tableOptions_mysql);
                $this->addPrimaryKey('professor_subject__pk', 'professor_subject_link', ['professor_id', 'subject_id']);

            }
        }

        /* MYSQL */
        if (!in_array('student', $tables))  {
            if ($dbType == "mysql") {
                $this->createTable('{{%student}}', [
                    'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
                    0 => 'PRIMARY KEY (`id`)',
                    'oib' => 'VARCHAR(11) NOT NULL',
                    'name' => 'VARCHAR(255) NOT NULL',
                    'surname' => 'VARCHAR(255) NOT NULL',
                    'address' => 'VARCHAR(255) NULL',
                    'post' => 'INT(11) NULL',
                    'location' => 'VARCHAR(252) NULL',
                    'date_of_birth' => 'DATE NULL',
                    'date_of_admission' => 'DATE NOT NULL',
                ], $tableOptions_mysql);
            }
        }

        $this->createIndex('idx_professor_id_9726_00','exam','professor_id',0);
        $this->createIndex('idx_student_id_9726_01','exam','student_id',0);
        $this->createIndex('idx_subject_id_9726_02','exam','subject_id',0);
        $this->createIndex('idx_subject_id_9756_03','professor_subject_link','subject_id',0);
        $this->createIndex('idx_UNIQUE_oib_9776_04','student','oib',1);

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_professor_9696_00','{{%exam}}', 'professor_id', '{{%professor}}', 'id', 'RESTRICT', 'CASCADE' );
        $this->addForeignKey('fk_student_9696_01','{{%exam}}', 'student_id', '{{%student}}', 'id', 'RESTRICT', 'CASCADE' );
        $this->addForeignKey('fk_subject_9696_02','{{%exam}}', 'subject_id', '{{%subject}}', 'id', 'RESTRICT', 'CASCADE' );
        $this->addForeignKey('fk_professor_9756_03','{{%professor_subject_link}}', 'professor_id', '{{%professor}}', 'id', 'RESTRICT', 'CASCADE' );
        $this->addForeignKey('fk_subject_9756_04','{{%professor_subject_link}}', 'subject_id', '{{%subject}}', 'id', 'RESTRICT', 'CASCADE' );
        $this->execute('SET foreign_key_checks = 1;');

        $this->execute('SET foreign_key_checks = 0');
        $this->insert('{{%exam}}',['id'=>'1','student_id'=>'2','professor_id'=>'2','subject_id'=>'2','mark'=>'5','date'=>'2019-05-13']);
        $this->insert('{{%exam}}',['id'=>'2','student_id'=>'4','professor_id'=>'3','subject_id'=>'1','mark'=>'4','date'=>'2019-05-08']);
        $this->insert('{{%exam}}',['id'=>'3','student_id'=>'2','professor_id'=>'3','subject_id'=>'4','mark'=>'3','date'=>'2019-05-16']);
        $this->insert('{{%exam}}',['id'=>'4','student_id'=>'3','professor_id'=>'3','subject_id'=>'4','mark'=>'2','date'=>'2019-05-11']);
        $this->insert('{{%professor}}',['id'=>'2','oib'=>'65100455097','name'=>'Xavier','surname'=>'Professor','address'=>'','post'=>'','location'=>'Dugopolje','date_of_birth'=>'2019-05-01']);
        $this->insert('{{%professor}}',['id'=>'3','oib'=>'01831868962','name'=>'Oak','surname'=>'Profesor','address'=>'','post'=>'','location'=>'Dugo Selo','date_of_birth'=>'1980-05-18']);
        $this->insert('{{%professor}}',['id'=>'4','oib'=>'45522673929','name'=>'Ivo','surname'=>'Sanader','address'=>'321321','post'=>'321321','location'=>'321321','date_of_birth'=>'2019-05-15']);
        $this->insert('{{%professor_subject_link}}',['professor_id'=>'2','subject_id'=>'2']);
        $this->insert('{{%professor_subject_link}}',['professor_id'=>'2','subject_id'=>'4']);
        $this->insert('{{%professor_subject_link}}',['professor_id'=>'3','subject_id'=>'5']);
        $this->insert('{{%student}}',['id'=>'1','oib'=>'2147483647','name'=>'asd','surname'=>'asd','address'=>'asd','post'=>'123','location'=>'asd','date_of_birth'=>'2019-10-04','date_of_admission'=>'2019-05-29']);
        $this->insert('{{%student}}',['id'=>'2','oib'=>'05810587973','name'=>'PEro','surname'=>'Perić','address'=>'','post'=>'','location'=>'','date_of_birth'=>'','date_of_admission'=>'2019-05-05']);
        $this->insert('{{%student}}',['id'=>'3','oib'=>'27887771744','name'=>'Ivo','surname'=>'Ivić','address'=>'','post'=>'','location'=>'','date_of_birth'=>'','date_of_admission'=>'2019-02-12']);
        $this->insert('{{%student}}',['id'=>'4','oib'=>'15381725961','name'=>'bero','surname'=>'berić','address'=>'asd','post'=>'123','location'=>'123','date_of_birth'=>'2019-05-04','date_of_admission'=>'2018-12-01']);
        $this->insert('{{%subject}}',['id'=>'1','name'=>'Hrvatski','ects_points'=>'10','active'=>'active']);
        $this->insert('{{%subject}}',['id'=>'2','name'=>'Matematika','ects_points'=>'10','active'=>'active']);
        $this->insert('{{%subject}}',['id'=>'3','name'=>'Priroda','ects_points'=>'5','active'=>'inactive']);
        $this->insert('{{%subject}}',['id'=>'4','name'=>'Geografija','ects_points'=>'5','active'=>'active']);
        $this->insert('{{%subject}}',['id'=>'5','name'=>'Linearna Algebra','ects_points'=>'10','active'=>'active']);
        $this->execute('SET foreign_key_checks = 1;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "good luck";
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `exam`');
        $this->execute('SET foreign_key_checks = 1;');
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `professor`');
        $this->execute('SET foreign_key_checks = 1;');
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `professor_subject_link`');
        $this->execute('SET foreign_key_checks = 1;');
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `student`');
        $this->execute('SET foreign_key_checks = 1;');
        $this->execute('SET foreign_key_checks = 0');
        $this->execute('DROP TABLE IF EXISTS `subject`');
        $this->execute('SET foreign_key_checks = 1;');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190512_080816_initial cannot be reverted.\n";

        return false;
    }
    */
}
