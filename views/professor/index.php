<?php

use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\AutoComplete;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchProfessor */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Professors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="professor-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Professor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'oib',
            [
                "class" => yii\grid\DataColumn::className(),
                "attribute" => "fullname",
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'fullname',
                    'clientOptions' => [
                        'source' => \app\models\Professor::find()->select(['concat(name, " ", surname) AS value', 'concat(name, " ", surname) AS label'])->orderBy('name')->asArray()->all(),
                    ],
                    'options' => ['class' => 'form-control'],
                ]),
                'value' => function ($model) {
                    return $model->getFullName();
                },
                "format" => "raw",
                'label' => "Professor",

            ],
            'address',
            [
                'label' => 'Subjects',
                'value' => function ($model)
                {
                    return implode(', ', $model->getSubjects()->select('name')->column());
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
