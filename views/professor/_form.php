<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Professor */
/* @var $form yii\widgets\ActiveForm */
/* @var $update boolean */
?>

<div class="professor-form">

    <?php $form = ActiveForm::begin([
        'id' => 'dynamic-form',
        'enableClientValidation' => true
    ]); ?>

    <?= $form->field($model, 'oib')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'post')->textInput() ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_of_birth')->widget(\yii\jui\DatePicker::class, [
        'value' => date('Y-m-d'),
        'dateFormat' => 'dd-MM-yyyy',
    ]) ?>

    <?php
    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => $model->getProfessorSubjectLinks(),
        'pagination' => [
            'pageSize' => 20,
        ],
    ]);
    if($update) {

        echo \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'subject.name',
                    'label' => 'Subject'
                ],
                'subject.ects_points',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'Actions',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', $url, [
                                'title' => Yii::t('app', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete?'),
                                'data-method' => 'post', 'data-pjax' => '0',
                            ]);
                        }

                    ],
                    'urlCreator' => function ($action, $model) {
                        if ($action === 'delete') {
                            $url = Url::to(['professor/delete-subject', 'subject_id' => $model->subject->id, 'professor_id' => $model->professor->id ]);
                            return $url;
                        }
                    }
                ]
            ],
        ]);
    }
    ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= ($update) ? Html::a('Add subject', ['/professor-subject-link/create', 'professor_id' => $model->id], ['class' => 'btn btn-primary']) : false; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
