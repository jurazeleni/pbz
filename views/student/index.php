<?php

use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\AutoComplete;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchStudent */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Student', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'oib',
            [
                "class" => yii\grid\DataColumn::className(),
                "attribute" => "fullname",
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'fullname',
                    'clientOptions' => [
                        'source' => \app\models\Student::find()->select(['concat(name, " ", surname) AS value', 'concat(name, " ", surname) AS label'])->orderBy('name')->asArray()->all(),
                    ],
                    'options' => ['class' => 'form-control'],
                ]),
                'value' => function ($model) {
                    return $model->getFullName();
                },
                "format" => "raw",
                'label' => "Student",

            ],
            'address',
            //'post',
            //'location',
            //'date_of_birth',
            //'date_of_admission',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
