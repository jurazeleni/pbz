<?php

use app\models\Professor;
use app\models\Student;
use app\models\Subject;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Exam */
/* @var $professor app\models\Professor */
/* @var $student app\models\Student */
/* @var $subject app\models\Subject */
/* @var boolean $errorProfessorSubject
/* @var $form yii\widgets\ActiveForm */
?>

<div class="exam-form">

    <?php
        if ($errorProfessorSubject) {
            echo '<div class="alert alert-danger" role="alert">This professor doesnt teach this subject!</div>';
        }
    ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'student_id')->hiddenInput()->label(false) ?>
    <?php $data = \app\models\Student::find()
        ->select(['concat(name, " ",  surname) as  label', 'concat(name, " ",  surname) as  label','id as id'])
        ->asArray()
        ->all();

    echo '<label class="control-label" >Student:</label><br/>';
    echo AutoComplete::widget([
        'clientOptions' => [
            'source' => $data,
            'minLength'=>'3',
            'autoFill'=>true,
            'select' => new JsExpression("function( event, ui ) {
			        $('#exam-student_id').val(ui.item.id);
			     }")],
            'value' => ($student) ? $student->getFullName() : ''
    ]);
    ?>


    <?= $form->field($model, 'professor_id')->hiddenInput()->label(false) ?>
    <?php $data = \app\models\Professor::find()
        ->select(['concat(name, " ",  surname) as  label', 'concat(name, " ",  surname) as  label','id as id'])
        ->asArray()
        ->all();

    echo '<label class="control-label" >Professor:</label><br/>';
    echo AutoComplete::widget([
        'clientOptions' => [
            'source' => $data,
            'minLength'=>'3',
            'autoFill'=>true,
            'select' => new JsExpression("function( event, ui ) {
			        $('#exam-professor_id').val(ui.item.id);
			     }")],
            'value' => ($professor) ? $professor->getFullName() : ''
    ]);
    ?>

    <?= $form->field($model, 'subject_id')->hiddenInput()->label(false) ?>
    <?php $data = \app\models\Subject::find()
        ->select(['name as value', 'name as  label','id as id'])
        ->asArray()
        ->all();

    echo '<label class="control-label" >Subject:</label><br/>';
    echo AutoComplete::widget([
        'clientOptions' => [
            'source' => $data,
            'minLength'=>'3',
            'autoFill'=>true,
            'select' => new JsExpression("function( event, ui ) {
			        $('#exam-subject_id').val(ui.item.id);
			     }")],
            'value' => ($subject) ? $subject->name : ''
    ]);
    ?>
    <div class="help-block"></div>


    <?= $form->field($model, 'mark')->textInput() ?>

    <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
        'value' => date('Y-m-d'),
        'dateFormat' => 'dd-MM-yyyy',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
