<?php

use app\models\Professor;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Exam */
/* @var $professor app\models\Professor */
/* @var $student app\models\Student */
/* @var $subject app\models\Subject */
/* @var boolean $errorProfessorSubject */

$this->title = 'Create Exam';
$this->params['breadcrumbs'][] = ['label' => 'Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exam-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'professor' => $professor,
        'student' => $student,
        'subject' => $subject,
        'errorProfessorSubject' => $errorProfessorSubject
    ]) ?>

</div>
