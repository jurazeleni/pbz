<?php

use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\AutoComplete;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchExam */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Exams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exam-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Exam', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?=  Html::hiddenInput('student_id ');?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                "class" => yii\grid\DataColumn::className(),
                "attribute" => "student_fullname",
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'student_fullname',
                    'clientOptions' => [
                        'source' => \app\models\Student::find()->select(['concat(name, " ", surname) AS value', 'concat(name, " ", surname) AS label'])->orderBy('name')->asArray()->all(),
                    ],
                ]),
                'value' => function ($model) {
                    return $model->student->name . ' ' . $model->student->surname;
                },
                "format" => "raw",
                'label' => "Student"
            ],
            [
                "class" => yii\grid\DataColumn::className(),
                "attribute" => "professor_fullname",
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'professor_fullname',
                    'clientOptions' => [
                        'source' => \app\models\Professor::find()->select(['concat(name, " ", surname) AS value', 'concat(name, " ", surname) AS label'])->orderBy('name')->asArray()->all(),
                    ],
                ]),
                 'value' => function ($model) {
                    return $model->professor->name . ' ' . $model->professor->surname;
                },
                "format" => "raw",
                'label' => "Professor"
            ],
            [
                "class" => yii\grid\DataColumn::className(),
                "attribute" => "subject_fullname",
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'attribute' => 'subject_fullname',
                    'clientOptions' => [
                        'source' => \app\models\Subject::find()->select(['name AS value', 'name AS label'])->orderBy('name')->asArray()->all(),
                    ],
                ]),
                'value' => function ($model) {
                    return $model->subject->name;
                },
                "format" => "raw",
                'label' => "Subject"
            ],

            'mark',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ],
        ],
    ]); ?>


</div>
