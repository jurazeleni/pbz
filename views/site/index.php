<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Welcome';
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-4 col-md-4 ">
            <div class="panel panel-info">
                <div class="panel-heading">Students</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-xs col-md text-center padded">
                            <a class="btn btn-info btn-block" type="submit" href="<?= Url::toRoute(['student/']);?>">List</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs col-md text-center">
                            <a class="btn btn-success btn-block" type="submit" href="<?= Url::toRoute(['student/create']);?>">New</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-4 col-md-4 text-center">
            <div class="panel panel-info">
                <div class="panel-heading">Professors</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-xs col-md text-center padded">
                            <a class="btn btn-info btn-block" type="submit" href="<?= Url::toRoute(['professor/']);?>">List</a>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-xs col-md text-center">
                            <a class="btn btn-success btn-block" type="submit" href="<?= Url::toRoute(['professor/create']);?>">New</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-4 col-md-4 text-center">
            <div class="panel panel-info">
                <div class="panel-heading">Subjects</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-xs col-md text-center padded">
                            <a class="btn btn-info btn-block" type="submit" href="<?= Url::toRoute(['subject/']);?>">List</a>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-xs col-md text-center">
                            <a class="btn btn-success btn-block" type="submit" href="<?= Url::toRoute(['subject/create']);?>">New</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center exam-div">
        <div class="col-md-6 justify-content-center align-self-center">
            <div class="panel panel-success">
                <div class="panel-heading">Exams</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-xs col-md text-center padded">
                            <a class="btn btn-info btn-lg btn-block" type="submit" href="<?= Url::toRoute(['exam/']);?>">List</a>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-xs col-md text-center">
                            <a class="btn btn-success btn-lg btn-block" type="submit" href="<?= Url::toRoute(['exam/create']);?>">New</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
