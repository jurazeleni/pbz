<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProfessorSubjectLink */

$this->title = 'Add subject to a professor';
$this->params['breadcrumbs'][] = ['label' => 'Professor Subject Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="professor-subject-link-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
