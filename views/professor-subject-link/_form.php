<?php

use yii\helpers\Html;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ProfessorSubjectLink */
/* @var $form yii\widgets\ActiveForm */

$professor_id = Yii::$app->request->get('professor_id');
$subject_id = Yii::$app->request->get('subject_id')
?>

<div class="professor-subject-link-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post'
    ]); ?>

    <?= $form->field($model, 'professor_id')->hiddenInput(['value' => $professor_id])->label(false); ?>
    <?= $form->field($model, 'subject_id')->hiddenInput(['value' => $subject_id])->label(false); ?>

    <fieldset>
    <?= Html::label('Professor'); ?>
    <?php if (!isset($professor_id)) {
           $data = \app\models\Professor::find()
                ->select(['concat(name, " ",  surname) as  label', 'concat(name, " ",  surname) as  label','id as id'])
                ->asArray()
                ->all();

            echo AutoComplete::widget([
                'clientOptions' => [
                    'source' => $data,
                    'minLength'=>'3',
                    'autoFill'=>true,
                    'select' => new JsExpression("function( event, ui ) {
                            $('#professorsubjectlink-professor_id').val(ui.item.id);
                         }")],
            ]);
        } else {
            echo Html::label(\app\models\Professor::findOne($professor_id)->fullname);
    }
    ?>
    </fieldset>

    <fieldset>
    <?php if (!isset($subject_id)) {
        $subjectData = \app\models\Subject::find()
            ->select(['name as value', 'name as label','id as id'])
            ->where(['status' => 'active'])
            ->asArray()
            ->all();

        echo 'Subject:' .'<br>';
        echo AutoComplete::widget([
            'clientOptions' => [
                'source' => $subjectData,
                'minLength'=>'3',
                'autoFill'=>true,
                'select' => new JsExpression("function( event, ui ) {
                            $('#professorsubjectlink-subject_id').val(ui.item.id);
                         }")],
        ]);
    } else {
        echo Html::label(\app\models\Subject::findOne($subject_id)->name);
    }
    ?>
    </fieldset>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
