<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProfessorSubjectLink */

$this->title = 'Update Professor Subject Link: ' . $model->professor_id;
$this->params['breadcrumbs'][] = ['label' => 'Professor Subject Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->professor_id, 'url' => ['view', 'professor_id' => $model->professor_id, 'subject_id' => $model->subject_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="professor-subject-link-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
