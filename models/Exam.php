<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exam".
 *
 * @property int $id
 * @property int $student_id
 * @property int $professor_id
 * @property int $subject_id
 * @property int $mark
 * @property string $date
 *
 * @property Professor $professor
 * @property Student $student
 * @property Subject $subject
 */
class Exam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exam';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'professor_id', 'subject_id', 'mark', 'date'], 'required'],
            [['student_id', 'professor_id', 'subject_id', 'mark'], 'integer'],
            [['date', 'professor_fullname'], 'safe'],
            [['professor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Professor::className(), 'targetAttribute' => ['professor_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Student ID',
            'professor_id' => 'Professor ID',
            'subject_id' => 'Subject ID',
            'mark' => 'Mark',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessor()
    {
        return $this->hasOne(Professor::className(), ['id' => 'professor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    public function beforeSave($insert) {

        if ($this->isNewRecord){
            $exists = ProfessorSubjectLink::find()->where([
                'professor_id' => Yii::$app->request->post('Exam')['professor_id'],
                'subject_id' => Yii::$app->request->post('Exam')['subject_id'],] )->exists();

            if(!$exists) {
                return false;
            }
        }

        if($this->date){
            $this->date = Yii::$app->formatter->asDate(strtotime($this->date    ), "php:Y-m-d");
        }

        return parent::beforeSave($insert);
    }
}
