<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "professor_subject_link".
 *
 * @property int $professor_id
 * @property int $subject_id
 *
 * @property Professor $professor
 * @property Subject $subject
 */
class ProfessorSubjectLink extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'professor_subject_link';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['professor_id', 'subject_id'], 'required'],
            [['professor_id', 'subject_id'], 'integer'],
            [['professor_id', 'subject_id'], 'unique', 'targetAttribute' => ['professor_id', 'subject_id'],'message'=>'Selected professor already teaches this subject'],
            [['professor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Professor::className(), 'targetAttribute' => ['professor_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'professor_id' => 'Professor ID',
            'subject_id' => 'Subject ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessor()
    {
        return $this->hasOne(Professor::className(), ['id' => 'professor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }
}
