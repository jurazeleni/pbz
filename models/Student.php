<?php

namespace app\models;

use Yii;

use app\components\helpers\OIB;

/**
 * This is the model class for table "student".
 *
 * @property int $id
 * @property int $oib
 * @property string $name
 * @property string $surname
 * @property string $address
 * @property int $post
 * @property string $location
 * @property string date_of_birth
 * @property string $date_of_admission
 *
 * @property Exam[] $exams
 * @property Exam[] $exams0
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['oib', 'name', 'surname', 'date_of_admission'], 'required'],
            [['oib', 'post'], 'integer'],
            [['oib'],'checkOib'],
            [['oib'],'string','min'=>11,'max'=>11],
            [['date_of_birth', 'date_of_admission', 'fullname'], 'safe'],
            [['name', 'surname', 'address'], 'string', 'max' => 255],
            [['location'], 'string', 'max' => 252],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oib' => 'Oib',
            'name' => 'Name',
            'surname' => 'Surname',
            'address' => 'Address',
            'post' => 'Post',
            'location' => 'Location',
            'date_of_birth' => 'Date of birth',
            'date_of_admission' => 'Date Of Admission',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExams()
    {
        return $this->hasMany(Exam::className(), ['student_id' => 'id']);
    }

    public function checkOib()
    {
        if (!OIB::check($this->oib)) {
            $this->addError('oib', 'OIB is no good');
        }

    }
    public function beforeSave($insert) {
        if($this->date_of_birth){
            $this->date_of_birth = Yii::$app->formatter->asDate(strtotime($this->date_of_birth), "php:Y-m-d");
        }
        if($this->date_of_admission){
            $this->date_of_admission = Yii::$app->formatter->asDate(strtotime($this->date_of_admission), "php:Y-m-d");
        }
        return parent::beforeSave($insert);
    }

    public function getFullName() {
        return $this->name . " " . $this->surname;
    }
}
