<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Professor;

/**
 * SearchProfessor represents the model behind the search form of `app\models\Professor`.
 */
class SearchProfessor extends Professor
{
    public $fullname;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'oib', 'post'], 'integer'],
            [['name', 'surname', 'address', 'location', 'date_of_birth', 'fullname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Professor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'oib' => $this->oib,
            'post' => $this->post,
            'date_of_birth' => $this->date_of_birth,
        ]);

        $query->andFilterWhere(['like', "concat(name, ' ', surname)", $this->fullname])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'location', $this->location]);

        return $dataProvider;
    }
}
