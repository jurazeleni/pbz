<?php

namespace app\models;

use app\components\helpers\OIB;
use Yii;

/**
 * This is the model class for table "professor".
 *
 * @property int $id
 * @property int $oib
 * @property string $name
 * @property string $surname
 * @property string $address
 * @property int $post
 * @property string $location
 * @property string date_of_birth
 *
 * @property Exam[] $exams
 * @property ProfessorSubjectLink[] $professorSubjectLinks
 * @property Subject[] $subjects
 */
class Professor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'professor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['oib', 'name', 'surname'], 'required'],
            [['oib', 'post'], 'integer'],
            [['oib'],'checkOib'],
            [['oib'],'string','min'=>11,'max'=>11],
            [['date_of_birth', 'fullname'], 'safe'],
            [['name', 'surname', 'address', 'location'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oib' => 'Oib',
            'name' => 'Name',
            'surname' => 'Surname',
            'address' => 'Address',
            'post' => 'Post',
            'location' => 'Location',
            'date_of_birth' => 'Date of birth',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExams()
    {
        return $this->hasMany(Exam::className(), ['professor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessorSubjectLinks()
    {
        return $this->hasMany(ProfessorSubjectLink::className(), ['professor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(Subject::className(), ['id' => 'subject_id'])->viaTable('professor_subject_link', ['professor_id' => 'id']);
    }

    public function checkOib()
    {
        if (!OIB::check($this->oib)) {
            $this->addError('oib', 'OIB is no good');
        }

    }

    public function beforeSave($insert) {
        if($this->date_of_birth){
            $this->date_of_birth = Yii::$app->formatter->asDate(strtotime($this->date_of_birth), "php:Y-m-d");
        }
        return parent::beforeSave($insert);
    }

    public function getFullName() {
        return $this->name . " " . $this->surname;
    }
}
