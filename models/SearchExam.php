<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Exam;

/**
 * SearchExam represents the model behind the search form of `app\models\Exam`.
 */
class SearchExam extends Exam
{
    public $professor_fullname;
    public $student_fullname;
    public $subject_fullname;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'student_id', 'professor_id', 'subject_id', 'mark'], 'integer'],
            [['date', 'professor_fullname', 'student_fullname', 'subject_fullname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Exam::find();
        $query->joinWith(['professor p', 'student st', 'subject su']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mark' => $this->mark,
            'date' => $this->date,
        ])->andFilterWhere(['like', "concat(p.name, ' ', p.surname)", $this->professor_fullname])
        ->andFilterWhere(['like', "concat(st.name, ' ', st.surname)", $this->student_fullname])
        ->andFilterWhere(['like', "su.name", $this->subject_fullname]);

        //die($query->createCommand()->sql);
        return $dataProvider;
    }
}
