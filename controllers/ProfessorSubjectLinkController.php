<?php

namespace app\controllers;

use Yii;
use app\models\ProfessorSubjectLink;
use app\models\SearchProfessorSubjectLink;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProfessorSubjectLinkController implements the CRUD actions for ProfessorSubjectLink model.
 */
class ProfessorSubjectLinkController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProfessorSubjectLink models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchProfessorSubjectLink();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProfessorSubjectLink model.
     * @param integer $professor_id
     * @param integer $subject_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($professor_id, $subject_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($professor_id, $subject_id),
        ]);
    }

    /**
     * Creates a new ProfessorSubjectLink model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProfessorSubjectLink();

        $professor_id = Yii::$app->request->get('professor_id');
        $subject_id = Yii::$app->request->get('subject_id');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (isset($professor_id)) {
                return $this->redirect(['professor/view', 'id' => $model->professor_id]);
            } else if (isset($subject_id)) {
                return $this->redirect(['subject/view', 'id' => $model->subject_id]);
            }

            return $this->redirect(['view', 'professor_id' => $model->professor_id, 'subject_id' => $model->subject_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProfessorSubjectLink model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $professor_id
     * @param integer $subject_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($professor_id, $subject_id)
    {
        $model = $this->findModel($professor_id, $subject_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'professor_id' => $model->professor_id, 'subject_id' => $model->subject_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProfessorSubjectLink model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $professor_id
     * @param integer $subject_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($professor_id, $subject_id)
    {
        $this->findModel($professor_id, $subject_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProfessorSubjectLink model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $professor_id
     * @param integer $subject_id
     * @return ProfessorSubjectLink the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($professor_id, $subject_id)
    {
        if (($model = ProfessorSubjectLink::findOne(['professor_id' => $professor_id, 'subject_id' => $subject_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
